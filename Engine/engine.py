import array as arr
from Engine.field import Field
from Engine.group import Group


class Engine:
    plansza = arr.array

    size = 0
    table = [[Field("*", 0) for x in range(size)] for y in range(size)]
    groups = [Group("*") for i in range(0)]

    # black_groups = [0][0][0]

    def __init__(self, size):
        self.size = size
        self.clear_table()
        print("size = " + str(self.size))

    @classmethod
    def set_size(cls, size):
        cls.size=size
        #cls.size = int(input("What size? : "))
        print("new size" + str(cls.size))
        cls.clear_table()

    @classmethod
    def clear_table(cls):
        cls.table = [[Field("*", 0, x, y) for x in range(cls.size)] for y in range(cls.size)]
        cls.groups = [Group("*") for i in range(0)]

    @classmethod
    def print_table(cls):
        print("\n")
        ix, iy = 0, 0
        line = "   "
        while iy < cls.size:
            line = line + str(iy)
            if iy < 10:
                line += "  "
            else:
                line += " "
            iy += 1
        print(line)
        for x in cls.table:
            line = str(ix)
            if ix < 10:
                line += "  "
            else:
                line += " "
            ix += 1;
            for y in x:
                line = line + str(y.color) + "  "
            print(line)
            # print("\n")

    last_black_x = size;
    last_black_y = size;
    last_white_x = size;
    last_white_y = size;

    black_score = 0;
    white_score = 0;

    @classmethod
    def put(cls, color, x, y):
        print(cls.size)
        if x >= 0 and x < cls.size and y > 0 and y < cls.size:
            if color == "B" and x == cls.last_black_x and y == cls.last_black_y:
                print("Cannot perform the same move")
            elif color == "W" and x == cls.last_white_x and y == cls.last_white_y:
                print("Cannot perform the same move")
            elif cls.table[x][y].color == "*":
                cls.table[x][y].color = color
                cls.check_group(color, x, y)
                if color == "B":
                    cls.last_black_x = x
                    cls.last_black_y = y
                else:
                    cls.last_white_x = x
                    cls.last_white_y = y
                #cls.take(color, x, y)
                cls.check_neighbours(color,x,y)
                return True
        else:
            print("Wrong coordinates!")
        return False

    @classmethod
    def check_group(cls, color, x, y):
        has_group=False
        if x<cls.size-1:
            if cls.table[x+1][y].color==color:
                if has_group==False:
                    cls.table[x][y].group_nr=cls.table[x+1][y].group_nr
                    print("added to " + str(cls.table[x][y].group_nr))
                    cls.groups[cls.table[x+1][y].group_nr].append_to_list(cls.table[x][y])
                    has_group=True
                else:
                    for i in cls.groups[cls.table[x+1][y].group_nr].list:
                        cls.groups[cls.table[x][y].group_nr].append_to_list(i)
                    cls.groups.pop(cls.table[x-1][y].group_nr)
                    cls.table[x + 1][y].group_nr = cls.table[x][y].group_nr
        if x>0:
            if cls.table[x-1][y].color==color:
                if has_group==False:
                    cls.table[x][y].group_nr=cls.table[x-1][y].group_nr
                    print("added to " + str(cls.table[x][y].group_nr))
                    cls.groups[cls.table[x-1][y].group_nr].append_to_list(cls.table[x][y])
                    has_group=True
                else:
                    for i in cls.groups[cls.table[x-1][y].group_nr].list:
                        cls.groups[cls.table[x][y].group_nr].append_to_list(i)
                    cls.groups.pop(cls.table[x-1][y].group_nr)
                    cls.table[x - 1][y].group_nr = cls.table[x][y].group_nr

        if y<cls.size-1:
            if cls.table[x][y+1].color==color:
                if has_group==False:
                    cls.table[x][y].group_nr=cls.table[x][y+1].group_nr
                    print("added to " + str(cls.table[x][y].group_nr))
                    cls.groups[cls.table[x][y+1].group_nr].append_to_list(cls.table[x][y])
                    has_group=True
                else:
                    for i in cls.groups[cls.table[x][y+1].group_nr].list:
                        cls.groups[cls.table[x][y].group_nr].append_to_list(i)
                    cls.groups.pop(cls.table[x][y+1].group_nr)
                    cls.table[x][y+1].group_nr = cls.table[x][y].group_nr
        if y>0:
            if cls.table[x][y-1].color==color:
                if has_group==False:
                    cls.table[x][y].group_nr=cls.table[x][y-1].group_nr
                    print("added to " + str(cls.table[x][y].group_nr))
                    cls.groups[cls.table[x][y-1].group_nr].append_to_list(cls.table[x][y])
                    has_group=True
                else:
                    for i in cls.groups[cls.table[x][y-1].group_nr].list:
                        cls.groups[cls.table[x][y].group_nr].append_to_list(i)
                    cls.groups.pop(cls.table[x][y - 1].group_nr)
                    cls.table[x][y-1].group_nr = cls.table[x][y].group_nr
        if has_group==False:
            new_size=cls.groups.__len__()
            cls.groups.append(Group(color, new_size))
            cls.groups[new_size].list.append(cls.table[x][y])
            cls.table[x][y].group_nr = new_size
            print("added to new " + str(new_size))

    @classmethod
    def check_neighbours(cls, color, x, y):
        if x < cls.size - 1:
            if cls.table[x+1][y].color!="*" and cls.table[x+1][y].color!=color:
                cls.count_liberties(cls.table[x+1][y].group_nr)
        if x > 0:
            if cls.table[x-1][y].color!="*" and cls.table[x-1][y].color!=color:
                cls.count_liberties(cls.table[x-1][y].group_nr)
        if y < cls.size - 1:
            if cls.table[x][y+1].color!="*" and cls.table[x][y+1].color!=color:
                cls.count_liberties(cls.table[x][y+1].group_nr)
        if y > 0:
            if cls.table[x][y-1].color!="*" and cls.table[x][y-1].color!=color:
                cls.count_liberties(cls.table[x][y-1].group_nr)

    @classmethod
    def count_liberties(cls, group_nr):
        liberties = 0
        for x in cls.groups[group_nr].list:
            print("group nr " + str(group_nr))
            print(str(x.x) + str(x.y))
            if x.x < cls.size - 1:
                if cls.table[x.x+1][x.y].color=="*" :
                    liberties+=1
            if x.x > 0:
                if cls.table[x.x-1][x.y].color=="*" :
                    liberties+=1
            if x.y < cls.size - 1:
                if cls.table[x.x][x.y+1].color=="*" :
                    liberties+=1
            if x.y > 0:
                if cls.table[x.x][x.y-1].color=="*" :
                    liberties+=1
        if liberties==0:
            print("0 liberties")
            for x in cls.groups[group_nr].list:
                x.color="*"
        else: print(str(liberties) + "liberties")
    @classmethod
    def take(cls, color, x, y):
        target = 0
        cur = 0
        if x + 1 < cls.size:
            if cls.table[x + 1][y] != color and cls.table[x + 1][y] != "*":
                target = 3
                cur = 0
                if x + 2 < cls.size:
                    if cls.table[x + 2][y] == color:
                        cur += 1
                else:
                    target += 1
                if y + 1 < cls.size:
                    if cls.table[x + 1][y + 1] == color:
                        cur += 1
                else:
                    target -= 1
                if y - 1 >= 0:
                    if cls.table[x + 1][y - 1] == color:
                        cur += 1
                else:
                    target -= 1
                if cur == target:
                    cls.table[x + 1][y] = color
                    cls.score(color)
        if x - 1 < cls.size:
            if cls.table[x - 1][y] != color and cls.table[x - 1][y] != "*":
                target = 3
                cur = 0
                if x - 2 >= 0:
                    if cls.table[x - 2][y] == color:
                        cur += 1
                else:
                    target -= 1
                if y + 1 < cls.size:
                    if cls.table[x - 1][y + 1] == color:
                        cur += 1
                else:
                    target -= 1
                if y - 1 >= 0:
                    if cls.table[x - 1][y - 1] == color:
                        cur += 1
                else:
                    target -= 1
                if cur == target:
                    cls.table[x - 1][y] = color
                    cls.score(color)
        if y + 1 < cls.size:
            if cls.table[x][y + 1] != color and cls.table[x][y + 1] != "*":
                target = 3
                cur = 0
                if y + 2 < cls.size:
                    if cls.table[x][y + 2] == color:
                        cur += 1
                else:
                    target -= 1
                if x + 1 < cls.size:
                    if cls.table[x + 1][y + 1] == color:
                        cur += 1
                else:
                    target -= 1
                if x - 1 >= 0:
                    if cls.table[x - 1][y + 1] == color:
                        cur += 1
                else:
                    target -= 1
                if cur == target:
                    cls.table[x][y + 1] = color
                    cls.score(color)
        if y - 1 < cls.size:
            if cls.table[x][y - 1] != color and cls.table[x][y - 1] != "*":
                target = 3
                cur = 0
                if y - 2 >= 0:
                    if cls.table[x][y - 2] == color:
                        cur += 1
                else:
                    target -= 1
                if x + 1 < cls.size:
                    if cls.table[x + 1][y - 1] == color:
                        cur += 1
                else:
                    target -= 1
                if x - 1 >= 0:
                    if cls.table[x - 1][y - 1] == color:
                        cur += 1
                else:
                    target -= 1
                if cur == target:
                    cls.table[x][y - 1] = color
                    cls.score(color)

    @classmethod
    def score(cls, color):
        if color == "B":
            cls.black_score += 1
            print("Black took white")
            print("Black score = " + str(cls.black_score))
        else:
            cls.white_score += 1
            print("White took black")
            print("White score = " + str(cls.white_score))

    move_nr = 0

    @classmethod
    def play(cls):
        while True:
            print("\nMove " + str(cls.move_nr))
            print("White")
            x = int(input("x: "))
            y = int(input("y: "))
            cls.put("W", x, y)
            cls.move_nr += 1

            cls.print_table()

            print("\nMove " + str(cls.move_nr))
            print("Black")
            x = int(input("x: "))
            y = int(input("y: "))
            cls.put("B", x, y)
            cls.move_nr += 1

            cls.print_table()
