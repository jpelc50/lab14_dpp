class Field:
    color = "*"
    group_nr = 0
    x=0
    y=0

    def __init__(self, color, group_nr, y, x):
        self.color = color
        self.group_nr = group_nr
        self.x=x
        self.y=y

    def set_color(self, color):
        self.color=color

    def set_gropu_nr(self, group_nr):
        self.group_nr=group_nr
