from Engine.field import Field


class Group:
    color = "*"
    nr = 0
    list = [Field("*", 0) for x in range(0)]

    def __init__(self, color, nr):
        self.color = color
        self.nr = nr
        self.list = [Field("*", 0) for x in range(0)]

    def append_to_list(self, field):
        self.list.append(field)

    def clear_list(self):
        self.list.clear()
