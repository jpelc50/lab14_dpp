# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from Engine.engine import Engine
# import Engine.Engine as engine
import os


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    black = "B"
    white = "W"
    eng = Engine(9)
    #eng.set_size()
    eng.clear_table()


    def cls():
        os.system('cls' if os.name == 'nt' else 'clear')


    menu = 1;

    cls()

    while menu != 0:
        menu = int(input("1. Put Black\n"
                         "2. Put white\n"
                         "3. Print table\n"
                         "4. Change size\n"
                         "5. Play\n"
                         "0. Exit\n:"))
        if menu == 1:
            x = int(input("x: "))
            y = int(input("y: "))
            eng.put(black, x, y)
        elif menu == 2:
            x = int(input("x: "))
            y = int(input("y: "))
            eng.put(white, x, y)
        elif menu == 3:
            eng.print_table()
        elif menu == 4:
            eng.set_size()
        elif menu == 5:
            eng.play()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
