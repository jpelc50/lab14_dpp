
import unittest

from Engine.engine import Engine

class Test(unittest.TestCase):

    Engine = Engine(9)


    def test_2putB(self):
        putB = self.Engine.put('B', 5, 5)
        self.assertEqual(putB, True)

    def test_3putA(self):
        putW = self.Engine.put('W', 5, 5)
        self.assertEqual(putW, False)

    def test_1setsize(self):
        self.Engine.set_size(9)
        print(str(self.Engine.size))
        self.assertEqual(self.Engine.size, 9)


if __name__ == '__main__':
    import xmlrunner
    runner = xmlrunner.XMLTestRunner(output='test-reports')
    unittest.main(testRunner=runner)
    unittest.main()
